README
======

This git repository contains the notes for the advanced networking course.
The notes are written in [restructured text](http://docutils.sourceforge.net/rst.html) format and we use [sphinx](sphinx-doc.org) to produce the latex, HTML and epub versions.

We use several [sphinx](sphinx-doc.org) extensions :

 - [sphinx-mscgen](https://pythonhosted.org/sphinxcontrib-mscgen/)
   to produce time sequence diagrams with [mscgen
 - [sphinx-tikz](https://bitbucket.org/philexander/tikz) to produce figures by using
   the [PGF and tikz](http://sourceforge.net/projects/pgf/) latex packages
 - [mcq](https://bitbucket.org/u531355/sphinx-form/) for multiple choice questions
 - [sphinx-bibtex](https://sphinxcontrib-bibtex.readthedocs.org/en/latest/) to
   manage the references

Examples of the utilisation of several of these extensions may be found in the [CNP3 ebook](https://github.com/obonaventure/cnp3)


