.. syllabus documentation master file, created by
   sphinx-quickstart on Fri May  1 20:25:24 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Traffic engineering on OSPF
---------------------------

.. sectionauthor:: Vincent Sceraert <vincent.sceraert@student.uclouvain.be>
.. sectionauthor:: Gael Wittorski <gael.wittorski@student.uclouvain.be>

This section is largely based on the article: Performance of New Link State Advertisement Mechanisms in Routing Protocols with Traffic Engineering Extensions [#ospfte_figure]_

Traffic engineering over IP
^^^^^^^^^^^^^^^^^^^^^^^^^^^
In order to lower the operative cost of the Internet service providers (ISP's), several traffic engineering techniques arose to prevent waste of resources. These techniques promise optimization of the resources (links, routers, ...) utilization by evenly distributing or balancing the load of the traffic on the various links while providing a certain levels of services, or quality of the service (QoS) to the customers. Since the main target of most IP routing protocols is to achieve connectivity between networks while eliminating loops, most of these protocols cannot handle the traffic engineering requirements. In fact, if we base the traffic only on shortest-path, we can achieve serious issues such as congestion and some links being under used. The packets may use the same path, even if other slightly worst paths exist.

To solve such issues, new algorithms, based on reservation or QoS, emerged. However, deploying such techniques in the IP network requires either defining new routing protocols or adding new features to existing routing protocols. Luckily, *Open Shortest Path First* (OSPF) and *Intermediate System to Intermediate System* (IS-IS), which are the most used on Internet nowadays, can be upgraded to meet the traffic engineering requirements. These extensions allows the nodes to exchange messages about the topology of the network, resources availability or policies.

OSPF recap
^^^^^^^^^^
OSPF, developed in 1988 by the **IETF**, was designed for *autonomous system* (AS) and classified as an *interior gateway protocol* (IGP). The main function of OSPF is to distribute link state information in the network. The nodes will then make routing decisions based on a consistent view of the network. In order to compute the shortest-path, OSPF uses the **Dijkstra** algorithm (*shortest path first* (SPF) search). OSPF allows each node to send *link state advertisement* (LSA) packets, containing information on its interfaces (IP address, metrics, ...). The LSA packets are sent to nodes that are in the same AS only. This technique which consists of sending packets across to network to share information is called *flooding*. Initial exchange of LSAs allows routers to build an incremental view of the topology of the AS. This means each node needs to accumulate LSA in order to achieve a good approximation of the network. The LSAs are stored in the *link state database* (LSDB). The router compute their decisions based on their LSBD, independently of other routers decisions.

This version of OSPF does not fit the traffic engineering goals. The main reason is the limited amount of information exchanged. As we just said, the path decided on shortest-path on the router based on its knowledge of the topology and its metrics. OSPF-TE will try to adapt the simple OSPF and making it meet the traffic engineering requirements by adding new exchanged information. For example, characteristics such as **bandwidth availability** will be sent by the nodes. This means that the path is no longer based on shortest-path only but also based on constraints to maximize network resource utilization and traffic performance. The constraints can be on routers and/or links.

Even if OSPF-TE is still new in literature, it is already used by other protocols such as Multiprotocol Label Switching (MPLS) which try to provide connection-oriented services where QoS and TE can he enforced more effectively.

OSPF-TE
^^^^^^^
As previously mentioned, OSPF-TE will add new information about the links in the LSA packets. OSPF-TE uses the opaque LSA packet of type 10, which consists of a standard LSA header followed by a 32-hit aligned application specific information field that adds more attributes to OSPF LSAs.

.. figure:: lsa_10.png
    :width: 500
    :align: center
 
    LSA packet of type 10 Change

See the above figure reference : [#ospfte_figure]_.

Just like typical LSA, LSA type 10 packets use the flooding to populate the LSDB of the nodes. The *TE database* (TED) will contains the information on the traffic engineering attributes for each links.

The additional attributes may be used by OSPF directly or the application which will distribute it. Let's note that type 9 and 11 exist but only the type 10 was proposed for OSPF-TE. The added special type is a TLV triplet called a *link TLV*. The TLV construct is a method of encoding protocol messages in an extensible manner. Each parameter of the message contains three fields :

* a type **T**, an implementation specific value that identifies the type of information carried in the packet
* a length **L**, length of V in bytes
* a value **V**, actual value of the information type

For the purpose of traffic engineering, one new LSA called *TE-LSA* was defined. The TE-LSA describes routers, point-to-point links, and connections to multi-access networks in a way similar to a router LSA. For multi-access links, the existing type 2 network LSA is sufficient, so no additional LSA is defined for this purpose. TE-LSA begins with a standard OSPF LSA header, then a payload filled with TLV triplets. An LSA contains a single top-level TLV. Two top-level TLV types are defined :

* **router address TLV**
        specifies a stable IP address of the advertising router that is always reachable as long as there is any connectivity to the router (typically the loopback address).

        The header is the same as the LSA defined in OSPF but the type is set to 10, meaning it is an opaque LSA packet with area-flooding scope. Just like other opaque packet, the link state ID portion of the native OSPF header is divided into two fields:

        * an 8-bit opaque type field
        * a 24-bit opaque ID field

        All TE-LSAs have the opaque type field set to 1, and the opaque ID is referred to as the  instance in OSPF-TE. This allows nodes to differentiate TE-LSA packets without looking at the content of the packet.

* **link TLV**
        describe a single link with a set of sub-TLV. They describe finer-granularity of network topology changes. The current OSPF-TE proposal allows for only one link TLV to be carried in each LSA. The following sub-TLVs are defined :

        * **Link type** (1 octet) : defines the type of the link (Multi-access or Point-to-point)
        * **Link ID** (4 octets) : identifies the other end of the link : for point-to-point links, this is the Router ID of the neighbour and for multi-access links, this is the interface address of the designated router. 
        * **Local interface IP address** (4 *N* octets where *N* = number of addresses) : specifies the IP address or addresses of the interface corresponding to this link(all are listed in this sub-TLV).
        * **Remote interface IP address** (4 *N* octets where *N* = number of addresses) : specifies the IP address or addresses of the neighbour's interface corresponding to this link. With the local address, it is used to discern multiple parallel links between systems.
        * **Traffic engineering metric** (4 octets) : specifies the link metric for traffic engineering purposes.  It can be different than the standard OSPF link metric(In general, this metric is assigned by the network administrator.).
        * **Maximum bandwidth** (4 octets) : specifies the maximum that can be used on the link or advertising interface from the router originating the LSA to one of its neighbours. This is the true link capacity.
        * **Maximum reservable bandwidth** (4 octets) : defines the maximum bandwidth that may be reserved on a given link from the router originating the LSA to one of its neighbours. When link bandwidth over-subscription is allowed, the maximum reservable bandwidth may be greater than the maximum bandwidth. Let's note that the default value should be set to the maximum bandwidth.
        * **Unreserved bandwidth** (32 octets) : defines the amount of link bandwidth not yet reserved at each of the available eight priority levels. The values correspond to the bandwidth that can be reserved with a setup priority of 0 through 7 . The initial values are all set to the maximum reservable bandwidth. Afterwards, each value will be less than or equal to the maximum reservable bandwidth.
        * **Administrative group/resource class/color** (4 octets) : specifies the groups of the link. Each bit correspond to a group (least significant bit corresponds to 'group 0', most significant to 'group 31'). A link may belong to multiple groups (Most of the time, it is assigned by the network administrator).

Natives OSPF/OSPF-TE with RSVP-TVCR-LDP
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
As previously explain, OSPF only exchange information on routers and links existence, and their metrics. This means that if a router A wants to reserve bandwidth but an other router R already reserve enough to make the reservation of R non-satisfying, the reservation won't be possible because it will only consider the shortest-path.

With OSPF-TE however, this problem will be avoided. In fact, in the OSPF-TE implementation, update are sent :

* periodically or
* if there is a significant amount of change in the unreserved bandwidth

This means that the router will know that the shortest-path can satisfy the reservation of router A and will use another path.

Link State Update Performance in OSPF-TE
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
While allowing new capabilities for traffic engineering, OSPF-TE also introduce some new issues that need to be adressed. Among them is simply the overhead caused by the flooding of additional LSA to propagate specific information about the links. The other big issue is the nature of the information sent inside TE-LSAs which is dynamic an require timely LSAs update
in order to be usable for traffic engineering. The mechanisms used to trigger the LSAs update are very crucial for having a smooth operation of an OSPF-TE networks and different techniques exists in order to perform these updates.

Traffic overhead
^^^^^^^^^^^^^^^^

.. figure:: ospf-te-overhead.png
    :width: 500
    :align: center

..

See the above figure reference : [#ospfte_figure]_.


In the previous figure is showed some experiments comparing the traffic sent with OSPF and OSPF-TE enabled in different topologies. In the graphs represented, the protocol traffic sent by OSPF-TE is the sum of the native OSPF and the additional traffic generated by the TA-LSAs. What is important to notice from this experiments is that the main factor affecting the traffic overhead of OSPF-TE is the number of links per router in the network. We can recall from what we've explained about the OSPF-TE protocol that an LSA is flooded for each link. Unlike in native OSPF which is advertising LSA per router, the node degree will a very important factor of the traffic overhead for OSPF-TE. Albeit the overhead can be mitigated by splitting the domain into areas but the flooding operates only inside a local area therefore network-wide, traffic engineering information won't be propagated across other areas without implementing another mean to do the propagation resulting also in an overhead.

LSA update triggering mechanism
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The triggering mechanism for LSAs update is not part of the OSPF-TE specification but the performance of OSPF-TE depends rather strongly on it. A good triggering mechanism should maximize routing accuracy while minimizing the LSA traffic due to OSPF-TE. If we were to generate a new TE-LSA after every bandwidth reservation, we would maximize the routing accuracy but the network load would not be sustainable in large networks. That is the reason why need to use some slightly more sophisticated scheme in order to achieve a trade-off between routing accuracy and LSA flooding traffic. Three of the possible scheme are:
    1. **Periodic updates:** In this technique the router will generate a new LSA update periodically according to a predeterminate period. The newly generated LSA will be flooded regardless of whether or not it has new information. [#ospfte_figure]_
    2. **Delta tresholding:** A new LSA will be generated and flooded whenever the difference between the current link available bandwidth and the last flooded bandwidth exceeds a certain constant treshold.    [#ospfte_figure]_
    3. **Relative thresholding:** A new LSA will be generated and flooded whenever the difference between the current link available bandwidth and the last flooded bandwidth exceeds a certain percentage (Threshold Value) of the last flooded bandwidth, according to the following formula: Relative Treshold = Last Flooded Bandwidth - Current Unreserved Bandwidth. When the Relative Treshold is greater or equal to the Treshold Value then we would flood a new LSA.     [#ospfte_figure]_

.. figure:: trigger-perf.png
    :width: 500
    :align: center
 
See the above figure reference : [#ospfte_figure]_.

The previous figure shows some performance measurement with those three triggering scheme (with MinLsInterval and MinLsArrival set to zero). The left part shows the percentage of bad decision in regard with a certain parameterized value of the scheme used. A bad decisision occurs when the network perform over-reservation or under-reservation, that is it tries to reserve some bandwidth although it is not available or it assumes some bandwidth is used whereas it is available. From this figure we can deduce the following table:

.. figure:: trigger-perf-table.png
    :width: 500
    :align: center

See the above figure reference : [#ospfte_figure]_.

The important observation from this table is that "relative tresholding provides the same level of routing accuracy decisions as the other two mechanisms, but with substantially less LSA traffic"[#ospfte_figure]_. 

The result showed previously were conducted with MinLsInterval and MinLsArrival disabled. How enabling MinLsInterval will impact the routing performance of OSPF-TE is illustrated with the following two following figures

.. figure:: trigger-perf-minlsinterval.png
    :width: 500
    :align: center

See the above figure reference : [#ospfte_figure]_.

.. figure:: trigger-perf-table-minlsinterval.png
    :width: 500
    :align: center

See the above figure reference : [#ospfte_figure]_.

The table shows the reduction in the amount of flooding traffic based on the case 1 of the figure presented just before. "Enabling the two rate-limiting timers significantly affects the amount of flooding traffic, reducing it by as mush as 18,4 percent when the treshold is set to zero, but its effect becomes gradually less significant for higher treshold values. We can also see in the case 2 which studies the effect of shorter interarrival time between successive LSP requests that the routing accuracy decreases when link state changes are more frequent. Therefore for high-end or backbones routers were the links serve a large number of LSPs, using a larger relative treshold may improve both the flooding traffic and the routing accuracy"[#ospfte_figure]_.

.. rubric:: References

.. [#ospfte_figure] "Performance of New Link State Advertisement Mechanisms in Routing Protocols with Traffic Engineering Extensions", Hussein M. Alnuweiri, Lai-Yat Kelvin Wong, and Tariq A/-Khasib, University of British Columbia  IEEE Communications Magazine - May 2004.

Multiple choice questions
^^^^^^^^^^^^^^^^^^^^^^^^^

.. question:: ospfte_q1
        :nb_pos: 2
        :nb_prop: 4

        1. Why is not OSPF good for Traffic Engineering ?

        .. negative::
                OSPF is a link-state protocol

        .. positive::
                Some informations are not present in the OSPF exchanges

                .. comment::
                        Characteristics such as **bandwidth availability** will be sent by the nodes and used when the path is computed

        .. positive::
                OSPF only compute the shortest-path

                .. comment::
                        OSPF can't decide a different path if a link is saturate, it will still take the shortest-path based on the topology

        .. negative::
                OSPF is only for small network

.. question:: ospfte_q2
        :nb_pos: 1
        :nb_prop: 3

        2. Does OSPF-TE only exist in literature ?

        .. negative::
                yes

        .. negative::
                no but only by LDP

        .. positive::
                no, it is used by MPLS for instance

                .. comment::
                        It is also used in Resource Reservation Protocol (RSVP)

.. question:: ospfte_q3
        :nb_pos: 1
        :nb_prop: 4

        3. Which attribute is not part of the link TLV information ?

        .. negative::
                maximum bandwidth of the link

        .. negative::
                maximum reservable bandwidth

        .. positive::
                Bidirectional Link Loss

                .. comment::
                        link TLV contains Unidirectional Link Loss, Delay Variation, Residual Bandwidth, Available Bandwidth, Utilized Bandwidth, ... (see http://www.iana.org/assignments/ospf-traffic-eng-tlvs/ospf-traffic-eng-tlvs.xhtml for more information on TLS link informations)

        .. negative::
                the unreserved bandwidth

.. question:: ospfte_q4
        :nb_pos: 1
        :nb_prop: 3

        4. What is the main factor of traffic overhead in an OSPF-TE enabled network?

        .. negative::
                The number of routers in the network

        .. positive::
                The number of links per router in the network

        .. negative::
                The size of the network
