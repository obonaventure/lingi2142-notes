MPLS-TE in an Internet Service Provider's network
-------------------------------------------------
.. sectionauthor:: Julien Louette <julien.louette@student.uclouvain.be>

The following section is based on the article "Traffic Engineering with MPLS in the Internet" [#mplste_isp_f1]_.
We then complete it with some other comments, examples and images.

The aim of this section is to discuss the utilization of MPLS to do Traffic Engineering in the specific area of Internet Service Provider's networks (ISP).
To understand the aim of this subject, we first make a short remainder about MPLS, Constraint-based Routing and enhanced link state Interior Gateway Protocols (IGPs).

.. note:: Why using Traffic Engineering in an ISP network?

    Without Traffic Engineering, ISPs usually use IGPs that only compute *shortest paths* to forward traffic.
    The problem is that using shortest paths may cause two major problems:

    1. *congestion* on some links
    2. paths *over-utilized* while paths *under-utilized*

    By using TE, ISPs can have a better control on the traffic, and optimize the resource utilization.

.. note:: Why using MPLS, Constraint-based Routing and an enhanced link state IGP to do Traffic Engineering in an ISP network?

    It is difficult to use IGPs to do TE in large networks, for the 3 following reasons:

    1. paths may carry significantly more traffic than other paths, due to equal share of load among ECMPs (Equal-Cost Multi-Paths).

    *On the following picture, we can observe that the A-C link is higly loaded.
    All the links have a cost metric of one, and a same bandwidth. If we consider the traffic matrix described on the figure,
    we can observe that the server A sends 60% of its traffic to F. B sends 40% of its packets to D and 80% of the packets from C are destined to D.
    In this situation, the A-D link has to support a total load of 110%.*

    .. figure:: mpls_te_in_the_internet/fig-ECMP.png
        :width: 562
        :align: center

        ECMP

    2. due to ECMPs, load sharing cannot be done among multiple paths of different costs.

    3. modifying IGP metric to modify the different paths is difficult and tends to have side effects.

    *On the two following pictures, we can observe the result of a modification of IGP metrics on the GEANT network.
    These results comes from the following paper: "Providing Public Intradomain Traffic Matrices to the Research Community"* [#mplste_isp_f4]_.

    *To de these graphs, a measure of the maximum link utilization inside the GEANT network has been taken every 15 minutes, over a period of 4 months.
    The "cutoff" in the graph corresponds to a lack of measures for a period of 7 days.*

    *In the first figure, the graph represents the result of the measures, with the actual IGP weights of GEANT.*

    .. figure:: mpls_te_in_the_internet/fig2-change_IGP_metric_1.png
        :width: 310
        :align: center

        With actual weights

    *To obtain the second graph, a change has been made in the IGP metrics. The simulation has been made with the TOTEM toolbox* [#mplste_isp_f5]_ *(based on the previous measures).
    All the IGP link metrics have been changed with the Cisco default weights corresponding to* :math:`1/link capacity` *(named invcap)*.

    *On this second figure, we can observe that the maximum load inside the network might be smaller than the actual one
    This is simply the result of a better load balancing on the most loaded links.*

    .. figure:: mpls_te_in_the_internet/fig2-change_IGP_metric_2.png
        :width: 311
        :align: center

        With invcap weights

    The IETF (Internet Engineering Task Force) then introduces MPLS, Constraint-based Routing and an enhanced link state IGP.

MPLS
^^^^

MPLS has already been defined in detail in the preceding sections. Here's a reminder of the main points.

MPLS is based on the commutation label scheme. It extends routing with respect to packet forwarding and path controlling.
Labels are inserted at the ingress nodes, and removed at the egress nodes of the MPLS-capable domain.
A router that supports MPLS is called a *Label Switching Router* (LSR).
The ingress LSR will firstly classify the incoming packets, based on their headers and some other informations maintained in their local routing information base.
These packets will then be forwarded all along the MPLS-capable domain, based on the assigned label and the local routing information maintained by each LSRs along the path.

When leaving the domain, the MPLS header is removed.
A path between an ingress LSR and an egress LSR is called a *Label Switched Path* (LSP).
LSPs is used to talk about all the existing paths between ingress LSRs and egress LSRs.


To allow an optimal control of the different paths, the routers considers attributes and requirements specified in configuration statements for the LSP.
These attributes are considered when the device calculates a traffic-engineered path.
Some of these attributes are the following:

.. _lsp_attributes_table:

.. table:: LSP Attributes

    ================ =======
    Attribute Name   Meaning
    ================ =======
    Bandwidth        minimum requirement on the *reservable bandwidth* (RB) of a path for the LSP to be set up along that path
    Path attribute   to decide whether the path of the LSP should be manually specified or dynamically computed by Constraint-based Routing
    Setup Priority   to decide which LSP will get the resource when multiple LSPs compete for it
    Holding Priority to decide whether an established LSP should be preempted the resource it holds by a new LSP
    Affinity (color) administratively specified property of an LSP
    Adaptability     whether to switch the LSP to a more optimal path when one becomes available
    Resilience       to decide whether to reroute the LSP when the current path is affected by failure
    ================ =======

These attributes will be considered in the following sections.

Constraint-based Routing
^^^^^^^^^^^^^^^^^^^^^^^^

*Constraint-based Routing* (CBR or CR) is used to compute routes that are subject to constraints, such as those listed in Table :ref:`lsp_attributes_table`.
The aim of this routing is to consider more than network topology in computing routes.

In the following example, directly taken from the article "Traffic Engineering with MPLS in the Internet" [#mplste_isp_f1]_, we can observe two paths from A to C.
With the provided metrics, the shortest path between A and C is through the A-C link.
If we observe the reservable bandwidth on this shortest path, we can observe that its value is only (622-600) = 22Mbps.

.. image:: mpls_te_in_the_internet/fig-CBR.png
    :width: 329
    :align: center

Now, if we need a path that is able to support a LSP of 40Mbps, the Constraint-based Routing will rather select the A-B-C path.
Indeed, the shortest path does not meet the bandwidth constraint, but the A-B-C path will.

.. note:: Reservable bandwidth

    The *reservable bandwidth* (RB) of a link is simply equal to the *maximum reservable bandwidth* (MRB) set by network administrator, minus the total bandwidth reserved by all the LSPs traversing the link.

        :math:`RB = MRB - \sum_{LSPs} bandwidth_i`

    It is important to note that this value does not depend on the actual amount of available bandwidth on the link.

    Note that an operator could decide to set a maximum reservable bandwidth that is lower than the link bandwidth.
    This will happen if for example the operator wants to use this link for best effort traffic and wants to have some capacity available for this traffic on this link.

We can then observe that the CBR does not compute the LSP paths regarding the instantaneous residual bandwidth of the links.
This allows to reduce the probability of routing instability.

.. note:: Constraint-based Routing, online >< offline

    With *online* Constraint-based Routing, each router may compute paths for LSPs at any time.

    In contrast, with *offline* Constraint-based Routing, an offline server computes paths for LSPs periodically (hourly/daily).
    LSPs are then configured to take the computed paths.

Enhanced link state Interior Gateway Protocols
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Why an enhanced link state IGP?**
The utilization of Constraint-based Routing to compute LSP paths is based on some attributes (listed in Table :ref:`lsp_attributes_table`).
These attributes are additional to the "normal"/"classical" link state informations, and need to be propagated through the network (to the different LSRs).
To do this, we need an enhanced link state IGP.

In contrast with normal link state IGP, enhanced link state IGP will flood information more frequently.
Indeed, without topology change, there is no need for normal IGP flooding. It is really difficult and deprecated to change the metrics of the network.
On the other hand, changes in the reservable bandwidth or link color (for example) is much more common on an MPLS network, and can then trigger the enhanced IGP to flood information more frequently.

**When is it usefull to flood information?** We need to avoid excessive flooding. But we need accurate informations. A tradeoff must then be found.
For example, the changes should be flooded only when they are significant, and a timer should be used to set an upper bound on flooding frequency.

The process of forwarding table construction can be illustrated as follows:

.. image:: mpls_te_in_the_internet/fig-forwarding-table-construction.png
    :width: 578
    :align: center

.. note:: Advantages of using MPLS, CBR and Enhanced link state IGP to do Traffic Engineering

    - By setting the maximum reservable bandwidth (MRB) of each link, and by setting the bandwidth requirement for each LSPs, CBR will automatically avoid placing too many LSPs on any link. (Congestion avoidance)
    - If the traffic between two routers exceeds the capacity of any single path between those routers, then multiple LSPs can be configured. Load ratio of these LSPs can be specified as desired, so that the load can be distributed optimally: the load ratio of the LSPs are automatically derived from their bandwidth specification !
    - Explicit routes (ERs) can be specified for LSPs. This can be used to control traffic trajectory more precisely.
    - Per-LSP statistics can provide accurate end-to-end traffic matrix. This matrix has a very high value for network optimization.
    - Backup LSPs can be defined to efficiently react in case of link failure.

General issues of designing an MPLS system for TE
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. note:: Architecture of ISP Networks

    ISP networks are designed around Points-of-presence (POPs) and links interconnecting these POPs.

    A POP consists of a combination of one or more:

    - *access routers* (ARs) connected to customers,
    - *border routers* (BRs) connected to other ISPs,
    - *hosting routers* (HRs) connected to Web servers of companies such as Google,
    - *core routers* (CRs) connected to other POPs.

    All the packets from one POP to another POP must then first be sent to a CR, that will then forward these packets to the CRs in other POPs.

    .. _fig_ISP_POPs:

    .. figure:: mpls_te_in_the_internet/fig-ISP-POPs.png
        :width: 399
        :align: center

        ISP POPs

    A large ISP may have more than 30 POPs (for example, 36 POPs for the `GARR ISP (Italy) in 2011 <http://www.topology-zoo.org/maps/Garr201201.jpg>`_, `www.topology-zoo.org <http://www.topology-zoo.org>`_).
    In 2014, Belnet had more than 13 POPs in Belgium. These are located in Louvain-la-Neuve, Namur, Antwerpen, Brugge, Gent, Kortrijk, Liege, Mons, Arlon, Charleroi, Geel, Hasselt and Leuven.
    They are all connected to the core, with double 10GBit/s links connectivity.

    .. _belnet_POPs:

    .. figure:: mpls_te_in_the_internet/fig-Belnet-POPs.png
        :width: 500
        :align: center

        Belnet POPs [mplste_isp_f2]_

    Connected to international research networks, the Belnet network promotes international collaboration between research and teaching institutes.
    It is thus part of the GEANT3 European research network and provides access to other research networks, including the American Internet2 network.

    .. _geant3_network:

    .. figure:: mpls_te_in_the_internet/fig-geant3.png
        :width: 500
        :align: center

        GEANT 3 Network [mplste_isp_f3]_

In this section, we will discuss the different design parameters that need to be determined to build an MPLS system for Traffic Engineering [#mplste_isp_f1]_:

1. the geographical scope of the MPLS system
2. the participating routers
3. the hierarchy of MPLS system
4. the bandwidth requirement of the LSPs
5. the path attribute of the LSPs
6. the priority of the LSPs
7. the number of parallel LSPs between each endpoint pair
8. the affinity of the LSPs and the links
9. the adaptability and resilience attributes of the LSPs

These different parameters are decided by network administrators, as described below.

**1. The geographical scope of the MPLS system**

Determining the scope of an MPLS system should be driven by administrative policy.
If the capacity of a region is low, it makes sense to integrate the region to the MPLS system.
Similarly, the MPLS architecture could be very usefull for regions where the network architecture is irregular (as opposed to the regular architecture showed in Fig. :ref:`fig_ISP_POPs`).

**2. The participating routers**

It is important to closely determine all the participating routers in the MPLS system.
We can distinguish routers as: *ingress* LSRs, *transit* LSRs and *egress* LSRs.

The following factors must be considered:

- Some routers may be forbidden from participating in the system, because:
    - those routers cannot be trust
    - those routers do not have enough processing power and/or memory capacity.
- A tradeoff between the number of LSPs and efficiency of the links must be found.

It makes sense that more ingress and egress LSRs mean more LSPs and thus higher LSP-routing complexity.
On the other hand, higher link efficiency may be achieved, as the bandwidth requirements of the LSPs are logically smaller. Constraint-based Routing has more flexibility in routing the LSPs.

**3. The hierarchy of MPLS system**

For large ISPs, the hierarchy of the MPLS system is particularly important.
The two following patterns must be considered:

- full mesh between all LSRs of the system
- full mesh in regions of the system, and full mesh between these regions

The main problem of a full mesh is the number of links that exponentially increases with the number of routers.
For a large ISP, with a simple full mesh between all the LSRs, the MPLS system will be huge, and then very difficult to maintain and establish.

The second pattern allows to divide the system in multiple regions. Inside these regions, all the routers can be fully meshed. This forms the first layer of LSPs.
In each region, some routers are used to be interconnected with other regions. This forms the second layer of LSPs.

.. figure:: mpls_te_in_the_internet/fig-MPLS-hierarchy.png
    :width: 396
    :align: center

    Hierarchy of MPLS system

**4. The bandwidth requirement of the LSPs**

For this part of the requirements, we need to distinguish two cases.

Firstly, when the system is launched since a moment, as previously discussed, we know that a traffic matrix is created and available.
This matrix could easily be used to efficiently determine the bandwidth requirements of the different links.

The complexity of this tasks is about the moment where the system is launched for the first time, or when new components are added to the system.
A methodology has been defined, and is given in [#mplste_isp_f1]_. This will be discussed in the section :ref:`deploying_mpls_system`.

**5. The path attribute of the LSPs**

The network administrators can chose between manually or dynamically computed LSP paths.

As discussed earlier, Constraint-based routing can be online or offline.
If CBR is online, LSPs are dynamically computed, and manually define paths for some LSPs is difficult.
In contrast, if CBR is offline, manually computed LSPs are much easier.

This needs to be taken into account when chosing between online or offline CBR.

**6. The priority of the LSPs**

It is possible to give different priorities on the different links. This allows the network administrators to optimally give more importance to some LSPs.
For example, LSPs carrying more traffic than others can be given a higher priority, and be assumed to take a more optimal path.

Note that usually this priority depends on the type of traffic carried by the LSP more than the bandwidth.
For example, in a network that supports normal internet access and VPN services, the LSPs used for the VPN service will have a
higher priority than those that are used for the normal internet access.

**7. The number of parallel LSPs between each endpoint pair**

An endpoint pair is an ingress-egress pair. With a lot of interconnections, it is often possible to determine multiple physical paths between a same ingress-egree pair.
It make sense that these parallel paths can be used to spread the traffic load between the two endpoints.

**8. The affinity of the LSPs and the links**

Here, we are talking about to characterization of the physical link, and the LSP !
The colors of the two elements needs to be in adequation.
The affinity or color of a link and an LSP is an easy way to help CBR to compute some path, taking into account some administratively defined requirements.

For example, for large ISPs, we talked about regions and POPs. We can then distinguish links in the regions, and links between regions.
With affinity, network administrators can easily determine some physical links as "local links" and some others as "inter regional links".
Then, when setting LSPs as "regional", they will never traverse any inter-regional link.

**9. The adaptability and resilience attributes of the LSPs**

The aim of these parameters is about the ability to switch some LSPs to better paths, depending on the stability of the network.
This switching of LSPs to better paths is callend LSP *reoptimization*.

This is not always desirable. Indeed, switching a path may introduce routing instability.
The adaptability parameter is used to determine if reoptimization is allowed, and the frequence of those reoptimizations.

The resilience is used to allow an LSP to be rerouted when failure occurs along the path.
In case of failure, it is even possible to reroute LSPs regardless of their bandwidth and other constraints.

.. note:: An Example - The national MPLS system of GlobalCenter

    As an example, we can consider the GlobalCenter ISP. It is one of the 10 largest ISPs in the US, and it can be characterised by the following:

    - Approximately 50 POPs of more than 300 routers (> 15.000 routers).
    - An enhanced IS-IS is used as the enhanced IGP.
    - All routers also run BGP.
    - MPLS is deployed nation-wide: for Traffic Engineering and VPNs.
    - Approximately 200 routers participate in the MPLS system.
    - A full mesh between these 200 routers *would* result in 40.000 LSPs ! So,
    - a hierarchical MPLS system has been decided, with 2 layers of LSPs.
    - The nation-wide network is divided in 9 regions.
    - The number of LSRs in a region ranges from 10 to 50, and
    - the number of LSPs in a region ranges from 100 to 2500.
    - Intra-region traffic ranges from several Mbps to several Gbps.
    - Inter-region traffic is in the magnitude of several Gbps.

.. _deploying_mpls_system:

Deploying MPLS system in an evolutionary way
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The procedure described below is based on the experience of the authors of [#mplste_isp_f1]_.
They participate in the deployment of the GlobalCenter's MPLS system.

**Step 1: Statistics Collecting using MPLS LSPs**

This step is very important to retrieve valuable informations regarding the end-to-end traffic matrix.
It makes sense that Traffic Engineering is based on these statistics.
The more these data can be accurate, the more the traffic engineering may be optimal.
In a classical network, we can easily retrieve inbound and outbound traffic of the network interfaces.
But this does not provide enough information regarding the end points of the different traffic flows going throught these interfaces.
We can achieve this task with a well defined network of LSPs, without specifying their bandwidth requirement (for the moment).

The following example is very clear.
In the network illustrated in the figure below, we can easily observe the traffic on the link from A to B and on the link from B to C.
But we cannot deduce the final destination of this traffic!
In other words, even if we know that the traffic from router A to router B and from router B to router C are both 1Mbps, it is impossible to know how much traffic from A is destined to B and how much is destined to C.

.. figure:: mpls_te_in_the_internet/fig-statistics-lsps.png
    :width: 363
    :align: center

    Statistics collecting using MPLS LSPs

Now, if we define two LSPs: one from A to B and one from A to C, then the traffic from A to C will not enter the LSP from A to B.
The statistics of these two LSPs will then tell network administrators precisely how much traffic from A is destined to B and how much is destined to C.

**Step 2: Deploy LSPs with Bandwidth Constraint**

Now that we have accurate informations regarding flows of data, we need to specify the LSPs with their bandwidth requirement.
A common choice to define the bandwidth requirement is to use the 95-percentile of all measured rates over a period of time.

Online CBR will then compute paths for the LSPs, as previously described.
Then the network administrators can add some explicit routes, lower reservable bandwidth of some links and add parallel LSPs alon some lightly loaded alternative paths.

**Step 3: Periodic Update of LSP Bandwidth**

This step is necessary to maintain an optimal MPLS Traffic Engineered system, and must be done periodically (e.g. daily or weekly).
Indeed, the traffic in the network is subject to change and the distribution of this traffic can be modified all the time.

**Step 4: Offline Constraint-based Routing**

If optimal link efficiency is desired, offline Constraint-based Routing can be used to compute the paths for the LSPs.
Offline CBR will compute paths for LSPs periodically, e.g. daily.
It can have more informations about LSP requirements, link attributes and complete network topology. So, it will be able to find better LSP placement than online CBR, where every router finds paths for its LSPs separately based on its own information.

.. rubric:: References

.. [#mplste_isp_f1] "Traffic Engineering with MPLS in the Internet", Xipeng Xia, Alan Hannan, Brook Bailey, GlobalCenter Inc. and Lionel M. Ni, Michigan State University. IEEE Network, March/April 2000.
.. [#mplste_isp_f4] "Providing Public Intradomain Traffic Matrices to the Research Community", Steve Uhlig, Bruno Quoitin, University of Louvain-la-Neuve, Belgium and Jean Lepropre, Simon Balon, University of Liege, Belgium (`Link <http://inl.info.ucl.ac.be/publications/providing-public-intradomain-traffic->`_).
.. [#mplste_isp_f5] TOTEM, "A TOolbox for Traffic Engineering Methods," `http://totem.info.ucl.ac.be <http://totem.info.ucl.ac.be>`_, February 2005.
.. [#mplste_isp_f2] Belnet - Rapport annuel 2013
.. [#mplste_isp_f3] Belnet - Rapport annuel 2010


Multiple choice questions
^^^^^^^^^^^^^^^^^^^^^^^^^

:task_id: mplste_ispnetworks

.. question:: mplste_isp_q1
        :nb_pos: 2
        :nb_prop: 3

        1. Why using Traffic Engineering in an ISP network?

        .. positive:: This allows to avoid congestion on some links

        .. positive:: This allows to optimize the resource utilization

        .. negative:: Classical IGPs computing shortest paths are very insecure

.. question:: mplste_isp_q2
        :nb_pos: 1
        :nb_prop: 3

        2. The reservable bandwidth of a link ...

        .. positive:: ... is equal to :math:`MRB - \sum_{LSPs} bandwidth_i`

        .. negative:: ... is equal to :math:`MRB + bandwidth_i`

        .. negative:: ... depends on the actual amount of available bandwidth on the link.

.. question:: mplste_isp_q3
        :nb_pos: 2
        :nb_prop: 3

        3. The affinity attribute of an LSP ...

        .. positive:: ... is the same as the color attribute of an LSP.

        .. positive:: ... must be used in adequation with the affinity attribute of a link.

        .. negative:: ... doesn't allows the network administrator to restrict some LSPs to some POPs.

.. question:: mplste_isp_q4
        :nb_pos: 1
        :nb_prop: 3

        4. A hierarchical architecture for MPLS system ...

        .. positive:: ... is possible for more than 4 levels of hierarchy.

        .. negative:: ... is used to promote the utilization of a complete full-mush between a maximum of routers.

        .. negative:: ... is particularly not suitable to manage systems located on several POPs.
