.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. Use the sectionauthor directive to indicate the contributor to each section
.. _`MPLS-VPN`:


BGP/MPLS VPNs
=============

.. sectionauthor:: Houtain Nicolas,Kabasele Ndonda Nicolas,Malengreau Symeon, Gerondal Thibault, Magrofuoco Nathan, Ooghe Nicolas

This chapter will describe the BGP/MPLS VPNs.

.. For documentation about rst : http://docutils.sourceforge.net/rst.html

Reminder MPLS
-------------

Multiprotocol  Label  Switching  (MPLS)  is  a
lightweight tunneling technology, or label-switched path (LSP), used in
many service provider networks. 

The ingress (entry) router appends packets that enter the LSP with a label. At
each hop in the LSP, the router swaps the label; at the end of the LSP, then
the egress (exit) router, disposing of this label, can send the packet on its way.


.. note:: The tunnel is created by using an added label at the start of
 the tunnel and removed at the end. 

MPLS VPNs
---------

There exist different techniques for tunneling packets in a VPN, to encapsulate MPLS-labeled packets in IP. There is two key issues when discussing those techniques:

- Security
- Processing overhead

1. **MPLS-over-IP** : 
    The first technique consists to append a 20-byte IP header to an MPLS packet. The *source ip* is the address of the sending PE router and the *destination ip* is the address of the receiving router.
   
    The main drawback of this technique is that the egress PE router must look inside the payload of the packet to see if it comes from a legitimate source and to figure out how to redirect the packet towards the next router.
  
2. **MPLS-over-GRE** : 
    Another technique will use GRE (Generic Routing Encapsulation). Using GRE, the packet is considered to be the payload of any other kind of packet of the network layer. Concretly, you put a 20-byte IP header and a 16-byte GRE.
    
    A local PE router must define GRE tunnel interfaces for each remote PE router. Some implementations use a single "multipoint" GRE tunnel interface that leads to all remote PE routers.
    
    This gives the possibility to the ISP to give access to their network to clients that don't use the same IP architecture. 
    
    Still, GRE doesn't include any fields that can carry values to let egress PE routers identify trusted source or MPLS service contexts.
    
3. **MPLS-over-IPSec** : 
    IP Security tunnels (IPSec) is mainly used over unsecured networks. This protocol authentifies and crypts the data transmitted. 
    
    As usualy MPLS VPNs are deployed mainly over infrastructure that provider owns or trusts, it's considered to be overkill.
  
4. **MPLS-over-L2TPv3** : 
    A recent solution consists of encapsulating MPLS packets in L2TPv3 (Layer-2 Transport Protocol version 3), an extension of L2TPv2 traditionally used to tunnel PPP sessions through an IP network.
    
    It is composed of
	
    - A 20-byte IP delivery header with the sending and receiving PE routed IP addresses.
    
    - The session ID as a 32-bits generated number so that the egress PE routed can find the appropriate service context just by looking at the id.
    
    - An optional 64-bits cookie.
    
Multiplicity of service
~~~~~~~~~~~~~~~~~~~~~~~

Considering all the possibilities in terms of encapsulation, PE routers need a mechanism for *capabilities exchange*, so they can notify the tunnel parameters.

To address this requirement, we use Tunnel Subaddress Family Identifiers (SAFI), which allow the propagation of tunnel attributes along with each advertised routes between PE routers.


Introduction to VPN
-------------------

Virtual private networks (VPNs) are discrete network entities configured
and operated over a shared network infrastructure. More concretly they directly link computers with tunneling technology simulating a LAN between them.

History
~~~~~~~

The vast majority of MPLS VPNs are operated
over MPLS infrastructures.


<<<<<<< HEAD
Back in time, we used **Leased line VPN**. Customer sites were
interconnected via static virtual channels such as ATM (*asynchronous
transfer mode*) or PVC (*frame relay private virtual connection*)
through a layer 2 backbone network. So, individual sites were connected to
the PE by a layer 2 connection which is a very expensive architecture in
terms of provisionning, configuration and management.


Terminology
~~~~~~~~~~

.. image:: img/VPN_arch.png
    :scale: 70 %
    :align: center
 
 
+---------------------------------+--------------------------------+------------------------+
| Router                          | Usage                          | Packets used            | 
+=================================+================================+========================+
| **PE** : *Provider edge router* | connects customers to the      | IP packets, MPLS packets |
|                                 | provider's backbone network    |                        |
+--------+------------------------+--------------------------------+------------------------+
| **P** : *Provider  router*      | composes the backbone area and | IP packets, MPLS packets |
|                                 | must have less intelligence    |                        |
+--------+------------------------+--------------------------------+------------------------+
| **CE** : *Customer edge router* | connected to PE and managed by | IP packets              |
|                                 | the customer                   |                        |
+--------+------------------------+--------------------------------+------------------------+



Different types of IP VPNs
~~~~~~~~~~~~~~~~~~~~~~~~~~

There exist two main kind of VPNs :

- *user-to-site* : 
   a user connected to a site belonging to the company of his company.
  
        It only requires a constant number (O(1)) of tunnels to connect the user to the VPN.

- *site-to-site VPNs* : 
   can have from a handful to hundreds of geographically
   distinct sites with a few to thousands of users per site. A site
   could even be a separate ISP network.
   
        Establishing connectivity between all sites within a single VPN
        requires a corresponding set of VPN tunnels ranging from O(N) to
        O(N^2), where N is the number of VPN tunnel end points.

They are subdivided in four category : 
        
1. **Dial**  (*user-to-site*) : 
    Initially, VPNs consisted of privately operated network devices
    interconnected over a carrier’s dial-up or dedicated leased lines.
    There exists two main types for these :

    - Virtual private dial networks (VPDNs) targeted telecommuters who
      required convenient and secure accesses to their home network servers.

       Typically, it's a PPP session from the end user to the corporate
       gateway. The most used layer-2 tunneling protocol (L2TP) supports
       the tunneling of PPP sessions containing user data through the
       Internet. 

    - We can also use a IPsec (Ip Security) VPN targeted corporations requiring data
      integrity, authentication, and privacy when communicating across the
      public Internet. The IPsec tunnels provide this level of security by
      using a variety of protocol exchanges and encapsulations at tunnel
      end points to authenticate and encrypt user data packets forwarded
      across the shared Internet

2. **Broadband** (*user-to-site*) : It's really similar to **dial**.
    An emerging suite of layer-2 VPN (L2VPN) solutions aims to extend
    layer-2 LAN and WAN connectivity through an IP or a multiprotocol
    label-switching (MPLS) network. Then, telecommuters are trading their
    analog 56-Kbyte dial-up modems for broadband wirelines (such as cable
    modems and DSL) or wireless (such as 802.11) accesses to launch
    secured IPsec tunnels directly from their laptops to the corporate
    gateways. Indeed, this latter model removes any performance delta
    that might have existed between working at home and working in an
    office attached to the corporate LAN

3. **CE-based** (*site-to-site*) :
    It's the customer that must performs all VPN routing and tunnel setup. (From CE to CE)
    CE requires a separate tunnel to connect to other CE sites.

    The provider has no knowledge of a customer's VPN routing or
    addressing scheme and sees only normal IP packets. 

    Since different VPN sites are typically interconnected through the
    Internet (an unknown and distrusted interconnection of networks),
    CE-based VPNs often make use of cryptographic security to protect
    their intersite traffic.

    Drawback : it needs customers to acquire, configure and maintain
    expensive VPN gateway...


4. **PE-based** (*site-to-site*) :
    The provider plays the dominant role in facilitation intersite
    VPN connectivity (it manages everything). The tunnels is made from PE
    to PE.

    PE needs to maintain separate context for every supported VPN,
    to ensure the distribution of the IP reachability information between
    distant sites belonging to the same VPN, and to smartly forward
    the VPN traffic.

    Client can connect their CE to the nearest PE to use the VPN. All the configuration
    and management is done on the PE side.
    
    
    ISPs tend to prefer this approach as they make more money of it. There are two kind of VPNs in
    this category :

    - L2VPN : an extension of *layer 2* LAN and WAN connectivity, packets
       are forwarded based on their MAC address.

    - L3VPN : Packet are forwarded based on the VPN customer's internal
       routing information, typically the packet header IP address.


When providing a service such as VPN, an ISP must meet different 
requirements but mainly it must be sure that when managing 
several VPNs, a packet belonging to company A will not end up in
company B.


.. note:: In this summary, we only really explore the PE-based on
  L3VPN.


Layer-3 VPN
-----------

Components
~~~~~~~~~~

- **Provider backbone network** : which is the set of P devices that
  forward or switch packets between PE. 
    *They do not contain any VPN information*

- **PE devices** : The PEs support and maintain separate per-VPN
  routes database that contains informations about the VPN’s topology
  (for example, VPN routers). A PE only needs a database for those
  provider-supported VPNs that have one or more directly attached links
  to a CE belonging to the VPN. The VPN databases are isolated from each
  other and from the provider’s internal network topology database
  inside the PE.

- **CE-PE VPN route exchange** : The CE can advertise routes within its
  local site to the PE and can receive routes from
  the PE collected from other PEs and CEs elsewhere in the VPN. (*by using border gateway protocol or BGP*)

- **Inter-PE VPN route exchange** : This ensures that each PE will
  have the correct VPN routes to forward packets
  to a specific VPN site. 

- **Inter-PE VPN tunnels** : Note that if the tunnel header uses a
  demultiplexing field, the PEs can forward packets belonging to
  different VPNs through the same VPN tunnel. This arrangement provides
  better scalability than per-VPN inter-PE tunnels.


------------------

.. note:: In this approach, the CE requires less configuration efforts.

The CE only needs to establish a single CE–PE link and routing adjacency
with the PE to connect to any other CE site in the VPN.

    Thus, in an L3VPN, the configuration effort on a per-CE basis
    is O(1), whereas in the CE-based model it is O(N). This can
    significantly benefit VPNs with large and geographically diverse
    CE populations and reduce the provisioning effort and time on the
    provider’s side.


-----------------

There is a benefit from a ISP and a customer point of view:

a. **ISP** : 
  - They can create new service offerings (and perhaps even
    bundle these new offerings with Internet access) over a single network
    infrastructure. (*something they couldn’t do if they were just
    offering VC connectivity or generic Internet routing.*)

     In opposition, to add a new site in the CE-based model, a
     provider might have to provision multiple VCs or tunnels to
     other CE sites (a time-consuming task).

  - Because a single PE can support multiple isolated
    instances of VPN routing and forwarding, L3VPN also reduces
    infrastructure costs.

  - Moreover, a traditional frame relay or ATM
    carrier can transform itself into an L3VPN provider
    by deploying a set of PE routers at the edge and
    interconnecting them via VCs in the backbone.

b. **Customer** : 
  - Outsourcing intersite routing operations to the
    provider requires fewer physical routers and operations staff to manage
    the network, thus reducing operational and capital costs.

  - Customers need not maintain sophisticated and expensive multi-VPN
    tunnel CE devices.

  - They can also keep their private IP addressing scheme at each CE
    site because the provider will opaquely tunnel VPN packets through its
    backbone.

Implementation
~~~~~~~~~~~~~~

There is two approaches for implementing a PE-based L3VPN :

1. MPLS VPN based on BGP and MPLS (2547)
  On each PE-router, there is one or more VRF (one per connected site).
  A **VRF** (virtual routing and forwarding) is a routing table instance.
  VRFs on a PE router are independent from each other, because of that 
  they are able to distinguish which packets belong to which VPN.
  Indeed, when a PE router receives a VPN route from another 
  PE router, it comes with a **RD** (route distinguisher). It's a 
  8-bytes field which is prefixed to the customer address. It allows
  to use the same IP prefix for different VPNs. This new address is
  called a VPN-IP address and because this new address is
  advertised with the default IP address, the protocol
  **MB-BGP** (Multiprotocol BGP) 

  When advertising a route, a PE router will attach a **RT** (route target)
  to it. A route target is labeled by PE-router to identify to which VRF
  the new route must be stored. It is also useful to define different 
  exporting/importing policies according to the RT.
  

2. Virtual routers
   In this approach, the PE router runs a VR instance or context.
   A VR emulates a physical router and works in the same way,
   it can forward packet and exchange route. 
   
   To achieve the separation of VPN contexts, it needs a 
   VR per supported VPN on every PE. To be able to exchange route
   and forward packet, VR are connected with each other through 
   tunnels. VR used a VPN ID to notify to which VPN does a route 
   belong to. Nowaday, VR are not widely used.
   
  



.. image:: img/L3VPN.png
    :scale: 70 %
    :align: center

In the figure above:

- The CE2 site advertises the 10.1.3/24 to the PE router.
- The PE2 router adds a RD of 003 and adds the RT 'red' before
  advertising it to the PE1. It also adds a label equal to L2
  and sets is loop-back addresss as the BGP next hop.
- When the route arrives at PE1, it compares for each VRF the RT value
  with the RT of the route. Because the RT of VRF1 matches, the route
  is stored in the VPN (the import policy of VRF1 allows RT='red').
- PE1 will then advertise this route to CE1 with its own address
  serving as the next hop.

The PE and P router have automatically build a separate MPLS path
connecting PE1 to PE2 so the VPN is ready to forward packets from CE1 to CE2.
When an host in CE1 site needs to send packet to an host in CE2 site:

- The packet will arrive in PE1. PE1 will look in the VRF1 to see which
  label it has to put on the packet (it'll be L2). It will then do a 
  lookup on its forwarding table to see how to reach PE2 (it'll be L4).
- The packet will then be forwarded based on the label put over the
  packet header. When the packet arrives to PE2, the label on the top
  is L2, so it knows that the packet must be forwarded to CE2. 
  It will set the packet to its original IP format before sending
  to CE1.





Tunneling
---------
As mentionned earlier, tunneling is a good way for ensuring privacy over
the internet, sensitive data can be transferred from a site to another one
in a secure way.

Usually the ISPs set the PE router as the tunnel's start/end point and they are the ones in charge of encapsulating the packet.


.. Danger::
    * same prefix from different networks
    * Full mesh vs hub/spoke topology


.. todo:: Full mesh vs hub/spoke topology



Tunneling technique
~~~~~~~~~~~~~~~~~~~

MPLS is used to forward packet with label and LDP/RSVP-TE designed two
protocols to signaling label.


+-------------+---------------------+---------------------+
|             | LDP                 | RSVP-TE             |
+=============+=====================+=====================+
| Topology    | Multipoint-to-point | Point-to-point      |
+-------------+---------------------+---------------------+
| Simplicity  | Very simple         | Complex             |
+-------------+---------------------+---------------------+
| Convergence | Low                 | Fast                |
+-------------+---------------------+---------------------+
| Qos         | Relative            | Guarantees possible |
+-------------+---------------------+---------------------+


RSVP allows to make reservation in Qos



iBGP organisation
~~~~~~~~~~~~~~~~~

iBGP is used to advertise VPN routes.

Routes advertising are done according to the kind of router involved:

- **CE-PE**:
  These routers use a dynamic routing protocol (BGP for example) to 
  exchange VPN route. The CE router advertise the routes within its
  local site to the PE routers and can receive the route that the PE routers
  collected from other PE routers.
- **PE-PE**:
  PEs routers exchange VPN routes via MP-BGP. They advertise the routes they
  learned from the CE routers connected to them. Because different
  VPN can use the IP prefix, PE routers use the RD to advertise to which
  VPN the address belongs to.

  To improve the scalability, technique like Route Reflector can be 
  used (which come with the possibility of deflection)
- **P**:
  These routers does not run MPBGP and are no aware of the existence 
  the VPNs.


Scalability implications of VPN
-------------------------------

     
Be aware that the general strategy is to concentrate the VPN intelligence at 
the PE. 

Important quantifier of scalability :

- the number of VPN supported
- the number of customer site than one VPN can contain
- the amount of information to maintain per customer site
- the amount of information to exchange per VPN (and the frequency of this exchange)

In a CE-Based VPN,the ISP does not configure the CE router manually. 
They use a central management system to be able to configure large 
VPN in a short amount of time.

In a PE-Based VPN, because all the intelligence is placed on 
the PE router, the number,the size of VRF and the size of 
the forwarding must be kept to a reasonable size. This size
will depend on how the customer will design their private 
network.  



Flow of information
-------------------

It's really important to understand the interaction between the different protocol :

- The first one is a routing protocol in the backbone to know how to reach the other router.
  In the most case, it's a link-state routing. 

- When the routing protocol converge, the PE router can establish iBGP session to
  exchange information about the VPN. Typically, a iBGP UPDATE message is send to all PE
  when a PE router receive a new adresse from a CE.
    
    Note: The PE router use import rule to know is the iBGP message is useful (when the
    RT corresponding to a VRF)

- LDP or RSVP-TE is used to distributed the label between PE and construct the 
  associated table.

- MPLS is used to encapsulated the IP packet and send it to the right PE. 

.. same prefix


Question
---------

.. include:: question.rst



References
---------

 - :rfc:`2784` for Generic Routing Encapsulation (GRE)
 - :rfc:`2547` BGP/MPLS VPNs


