.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. Use the sectionauthor directive to indicate the contributor to each section

Policing and shaping
--------------------
.. sectionauthor:: Sebastien Duval & Benoît Duhoux

In this section, we first explain the token bucket algorithm. Then we will explain two techniques that use the QoS token bucket: shaping and policing.
The difference between the two techniques is the following: for the shaping, the excedents packets are stored in a buffer while the policing drops them in case of excedents.

Token bucket algorithm
``````````````````````
The token bucket is based on a bucket with a size :math:`b` which contains :math:`n` fixed-size (:math:`=t`) tokens. A packet of size :math:`p` is sent if :math:`n*t \geq p`. If there are not enough tokens, the packets will be dropped or tagged to prevent congestion. To regenerate tokens, the algorithm is based on a rate :math:`r` (e.g.: the algorithm will generate 1 token of 50 bytes per second)and thus we will have a token generation all :math:`\frac{1}{r}` seconds. Tokens will be generated even if there are no packets to be transmitted. If the number of tokens reaches the size of the bucket, the additional token will be dropped. Figure [#ref_pict_TB]_ below illustrates the basic concept of the token bucket algorithm.


.. image:: pictures/token_bucket.PNG
	:align: center
	:alt: token bucket

In the codes below [#pseudo_codes]_, :math:`C` is the number of tokens in the bucket and :math:`B` is the maximum of tokens in the bucket.
The code of the token generation is:
::

	C = B; 
	every 1/R second do {  
		if (C < B) 
			C = C + 1; 
	} 

The code when the arrival of a packet :math:`P` with length :math:`L` is :
::

	if (L <= C) { /* packet is accepted */ 
		C=C-L; 
	} else { 
		/* packet is discarded */ 
	} 

This algorithm allows to vary the output rate as a function of the bucket's size. The average rate (:math:`AV`) is the maximum size of the bucket (:math:`MaxSB`) divided by the time interval (:math:`I`) to create new tokens. The formula is : :math:`AV = \frac{MaxSB}{I}`

The bigger the bucket, the greater the rate of tokens produced. That way, more packets can be sent.
If the size of the bucket is substantial while the rate is low, the transmission will be less fluid.
The opposite (small  bucket and high rate) allows the transmission to be fluid provided that :math:`p < n*t`.

A little animation to illustrate the concept is available at this `link <http://webmuseum.mi.fh-offenburg.de/index.php?view=exh&src=8>`_. 

Token bucket and shaping
````````````````````````
The traffic shaping is a management technique in the network traffic that. It is designed to control the traffic and follows policies between two network nodes.
One reason for its use is to avoid bootlenecks in the network and thus the creation of congestion.

Its policy is to delay the packets in order to meet the defined contracts. The output traffic will be in accordance with the maximum size of the bucket :math:`b` and the rate :math:`r` generated during the contract. The rate will be set to a specific rate or an average calculated based on the congestion level.
In addition to manage priorities, traffic shaping is based on the different scheduling algorithms `<schedulers.html>`_ depending on the implementation.

The code [#pseudo_codes]_ of the token bucket to transmit packets will be a little different.
Let :math:`L`, the size of returning packet. Let :math:`C`, the number of tokens in the bucket :
::

	if (L <= C) { 
		/* packet arrived on time */ 
   		C=C-L; 
		transmit_packet(); 
	} else {
		/* packet arrived too early  
  		 * delay packet inside buffer       
		 * until it becomes conforming      
		 */ 
 		while (C<L) { /* wait */ } 
 		/* now C=L and packet is conforming */ 
 		C=C-L; 
		transmit_packet();
	}

Token bucket and policing
`````````````````````````
The traffic policing is a management technique in network traffic. Its purpose is also to ensure that the contract is respected between network nodes. Again, the policing policy will be executed to control the outgoing traffic of the node.

Unlike traffic shaping, its policy is different. It will be based on 3 categories to mark a packet as conforming. The 3 categories are :

* Conforming : packets conform and can leave the network node. In this case, the packets will be transmitted.
* Exceeding : packets use all tokens capacity. Here, the packets will be marked to prevent other routers about the congestion.
* Violating : packets are totally outside the contract and therefore to be dropped.

In the case where you only use two categories, packets not included in the conforming category will be dropped.

There are various techniques of policing (variants of token bucket) : 

* Single rate, two-colors with one token bucket. : the idea is the same as for the token bucket. When a packet arrives, the policer will check if there are enough tokens available. If so, it sends it, otherwise it will drop. The figure below [#img_src]_ shows this concept.

.. image:: pictures/single_rate_two_colors.png
	:align: center
	:alt: Single rate, two-color with one token bucket

* Single rate, three-color with two token buckets : the idea is to use two token buckets in cascade. When a packet arrives in the first bucket (BC) it will either be marked as conforming or will fall into the second bucket. When a packet arrives in the second bucket (BE) it will either be marked as exceeding or violating. The figure below [#img_src]_ shows this concept.

.. image:: pictures/single_rate_three_colors.png
	:align: center
	:alt: Single rate, three-color with two token buckets

* Dual rate, three-color with two token buckets : The idea is to use two rates, committed information rate (CIR) and peak information rate (PIR). CIR is used for the BC bucket and PIR for the BE. PIR will generate tockens faster than CIR. When a packet arrives and if there is not enough tokens in the BC bucket, the packet will be marked violating. On the contrary, the packet arrives in the first bucket (BC). If there is not enough tokens in the BE bucket, the packet will be marked as exceeding. If there are enough tokens, the packet will be marked conforming. The figure below [#img_src]_ shows this concept.

.. image:: pictures/dual_rate_three_colors.png
	:align: center
	:alt: Dual rate, three-color with two token buckets

In Cisco, the mechanism of the policing is called CAR (Committed Access Rate). It can control the maximum transmission rate to the input or to the output of a network node. It can provide all traffic policing on an interface or on individual flows through an interface. CAR is based on the `classification parameters <classification.html>`_ such as IP precedence values, DSCP values, ...

Other mechanisms exist as Class-Based Policing for the Differentiated Services ...

Links
`````

.. rubric:: References

General:

- http://www.cisco.com/c/en/us/td/docs/ios/12_2/qos/configuration/guide/fqos_c/qcfpolsh.pdf

Token bucket:

- http://www.google.be/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&ved=0CCIQFjAA&url=http%3A%2F%2Fclass.ece.iastate.edu%2Fcpre458%2Fcpre558.F00%2Fnotes%2Frt-wan3.ppt&ei=zukwVe_wOMziatrugIAJ&usg=AFQjCNFi19rhSQCD_Y6y7eDbH1R2oqTbhA&bvm=bv.91071109,d.ZWU&cad=rja
- http://webmuseum.mi.fh-offenburg.de/index.php?view=exh&src=8
- http://fr.slideshare.net/vimal25792/leaky-bucket-tocken-buckettraffic-shaping

Shaping:

- http://docstore.mik.ua/univercd/cc/td/doc/product/lan/cat4224/sw_confg/traffic.pdf

Policing:

- http://networklessons.com/quality-of-service/qos-traffic-policing-explained/ 
- http://www.perihel.at/2/rno/04-QoS-Policing-and-Shaping.pdf
- http://routingnull0.com/2014/07/01/hour-465-single-rate-policers-and-dual-rate-three-color-policer/

Policing and Shaping:

- http://searchunifiedcommunications.techtarget.com/tip/Policing-and-shaping-within-QoS

.. rubric:: Footnotes

.. [#ref_pict_TB] From http://fr.slideshare.net/vimal25792/leaky-bucket-tocken-buckettraffic-shaping
.. [#pseudo_codes] Supplied codes come from slides of the first chapter on QoS in the LINGI2142 course at UCL given by Professor O. Bonaventure
.. [#img_src] From http://routingnull0.com/2014/07/01/hour-465-single-rate-policers-and-dual-rate-three-color-policer/

Questions:
``````````

Question 1: 
'''''''''''

.. question::
	:nb_pos: 1
	:nb_prop: 3

	What is the difference between the Traffic Shaping and the Traffic Policing ?
	
	.. negative::
		Traffic Policing is a technique that delays packets and Traffic Shaping is a technique that marks packets. 

	.. positive::
		Traffic Shaping is a technique that delays packets and Traffic Policing is a technique that marks packets.
		
	.. negative::
		There is no difference.

Question 2: 
'''''''''''

.. question::
	:nb_pos: 1
	:nb_prop: 3

	If 3 tokens of 70 bytes are generated every second and the size of each token is of 420 bytes, which of the following is true ?
	
	.. negative::
		The packets that are larger than 420 will always be sent.

	.. positive::
		If they are no packet to send, le number of token will not exceed 6.
		
	.. negative::
		Before 5 seconds, the number of packets that we could send will be 15.

Question 3: 
'''''''''''

.. question::
	:nb_pos: 1
	:nb_prop: 3

	(Single rate, two-color) If 2 tokens of 50 bytes each are generated every seconds and the size of the token bucket is 100 bytes. 5 packets of 50 bytes are send. The time are the following : the first two packets are send after 2 seconds, the last three packets are send after 3 seconds. What is true ?
	
	.. positive::
		The first 4 packets are marked with the conforming flag and the last with the exceeding flag.
	
	.. negative::
		The first 2 packets are marked with the conforming flag and the last 3 with the exceeding flag.

	.. negative::
		The 5 packets are marked with the conforming flag

Question 4: 
'''''''''''

.. question::
	:nb_pos: 1
	:nb_prop: 3

	(Single rate, three-color) If 2 tokens of 50 bytes each are generated every seconds and the size of the 2 token buckets (BC and BE buckets) is 100 bytes. 7 packets of 50 bytes are send. The time are the following: the first two packets are send after 2 seconds, the last five packets are send after 3 seconds. What is true ?
	
	.. negative::
		The first 6 packets are marked with the conforming flag and the last with the exceeding flag.

	.. negative::
		The 7 packets are marked with the conforming flag.

	.. positive::
		The first 4 packets are marked with the conforming flag, the 2 next with the exceeding flag and the last with the violating flag.

Question 5: 
'''''''''''

.. question::
	:nb_pos: 1
	:nb_prop: 3

	(Dual rate, three-color) If 2 tokens of 50 bytes each are generated every seconds for the CIR and that the PIR is made of 6 token of 100 bytes per seconds.
	The size of the BC bucket is of 200 bytes and the size of the BE bucket is of 800 bytes.
	Packets of variable length are send as follow : one packet of 300 bytes is send after 2 seconds. Then a packet of 500 bytes is send and then 5 packets of 100 bytes are send at second 3. How will the third packet be marked ?
	
	.. negative::
		The packet will be marked with and exceeding flag.

	.. positive::
		The packet will be marked with and conforming flag.

	.. negative::
		The packet will be marked with and violating flag.
