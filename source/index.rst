.. LINGI2142 documentation master file, created by
   sphinx-quickstart on Tue Feb 10 11:53:21 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

================================================
Computer Networks : Configuration and Management
================================================

Contents:

.. toctree::
   :maxdepth: 2

   BGP/index
   BGP2/index
   QoS1/index
   QoS2/index
   MPLS/index
   MPLS-TE/index
   MPLS-VPN/index
   Multicast1/index
   Multicast2/index
   Failures/index
   SR/index
   Datacenters/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

