.. sectionauthor:: Henri Crombe

Ethernet Multicast
==================

Ethernet multicasting relies on the layer 2 of the OSI model. At this level, frames are exchanged between hosts and MAC addresses are used to  uniquely distinguish every single device. Still at this level, unicast, broadcast and multicast traffic has to be distinguished. Broadcast traffic is easy to recognize because broadcasted frames always has a destination MAC address set to FF:FF:FF:FF:FF:FF. So, when a device receives a frame with this MAC address, it knows that it has to forward it to every device connected to it.  Multicast and unicast frames are distinguished by the Destination MAC address. The MAC address has a bit that is set to 0 for unicast and set to 1 to indicate that this is a multicast address.

For example :

- Imagine a frame going from host A to host B.
- Host A has 00:02:B3:3C:32:68 as MAC address.
- Host B has 00:A0:C9:AB:0E:8F as MAC address.

The frame going from A to B should have a source address set to 00:02:B3:3C:32:68 and a destination address set to 00:A0:C9:AB:0E:8F. When this frame reaches B, the Ethernet layer will look at the Destination MAC address. If the low-order bit of the high-order byte of the destination MAC address is set to 0, then the Ethernet layer knows that it's a unicast frame. In this case, the MAC address 00:A0:C9:AB:0E:8F looks like this in bits : 

   +------+------+------+------+------+------+------+------+------+------+------+------+
   | 0    | 0    | A    | 0    | C    | 9    | A    | B    | 0    | E    | 8    | F    |
   +------+------+------+------+------+------+------+------+------+------+------+------+
   | 0000 | 0000 | 1010 | 0000 | 1100 | 1001 | 1010 | 1011 | 0000 | 1110 | 1000 | 1111 |
   +------+------+------+------+------+------+------+------+------+------+------+------+

Note that the low-order bit of the high-order byte of the destination MAC address is set to 0. This means that this MAC address is a unicast one. If the bit was set to 1 then the MAC address would the multicast MAC address that it used to reach all the host.

Now, imagine that our host A wants to sends a multicast frame to a multicast group. The source and the receivers of this group has to agree on a MAC address that will be used to reach all the receivers of the group. The only restriction is that the low-order bit of the high-order byte of the destination MAC address is set to 1 (Note : Some of these multicast MAC addresses are reserved for multicast groups of specific vendors or MAC-level protocols. Internet multicast applications use the range 0x01-00-5E-00-00-00 to 0x01-00-5E-FF-FF-FF). Imagine that the group has agreed on the multicast MAC address 01:00:5E:00:00:05 : 

   +------+------+------+------+------+------+------+------+------+------+------+------+
   | 0    | 1    | 0    | 0    | 5    | E    | 0    | 0    | 0    | 0    | 0    | 5    |
   +------+------+------+------+------+------+------+------+------+------+------+------+
   | 0000 | 0001 | 0000 | 0000 | 0101 | 1110 | 0000 | 0000 | 0000 | 0000 | 0000 | 0101 |
   +------+------+------+------+------+------+------+------+------+------+------+------+

So, if a host C wants to receive frames it has to start listen for frames with 01:00:5E:00:00:05 as destination address and advertise its router that it has join the group with multicast address 01:00:5E:00:00:05. (Keep in mind that the destination MAC address  01:00:5E:00:00:05 is not the MAC address of a particular device but the MAC address that can be recognized by computers that are part of the multicast group). When the host A starts sending frames with destination MAC  address 01:00:5E:00:00:05, each device (interface) which is listening on the network will pick up the packet and verify if the destination address matches any of the multicast MAC addresses. If yes, the packet is pass to the upper layer for further processing.

