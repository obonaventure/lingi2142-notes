.. sectionauthor:: Hussein Bahmad

Internet Group Management Protocol
==================================

IGMP is a mechanism in IPv4 that helps routers to keep track of all multicast members for a given multicast group on the networks of the router. In IPv6, those functions are handled by the `Multicast Listener Discovery` protocol (MLD).

How does it work ?
------------------

The following explanation concerns the second version of IGMP (IGMPv2) which is nowadays the most widely used version.

When a host joins or leaves a multicast group, it has to inform the directly connected router. In case of join, the host must inform the router using multicast "Join message" (also called membership report message) that it want to receive packets from a specific multicast address.  If the router is not already connected to the distribution tree, it must use multicast routing protocol to get connected. 

The router does not keep track of the number of the hosts that are members of a given group. It simply keep a simple record, if there is at least one multicast group network on the directly connected network. Once a multicast packet arrives at the router, the router will forwards this packet to all hosts that are interested by a given multicast group address in the directly connected networks.  Therefore, a host with multiple interfaces may join a multicast group on any of its interfaces.

.. figure:: imgs/igmp-joining.png
   :align: center
   :scale: 70

   Joining a group [figmpjoin]_

..

The routers have to send periodically (by default every 125 seconds) an IGMP general query to all hosts that are on the same network (broadcast) in order to check if there are hosts that still members of a certain multicast group. The hosts that are members of the given multicast group will answer to the query with a "Join message". In case of multiple multicast group members, only one has to respond to the query. And if the router does not receive response from anyone, it assumes that there is nobody and it can eventually decide to stop forwarding multicast packets on the specific network.  In certain case some hosts sends a "leave message" (IGMPv2) to indicate to the router that it want to leave a given multicast group. This operation can be slow using IGMP Version 1.


Characteristics
---------------

**Mechanism**

 1. When a host want to join a specific group, it has to program its Ethernet interface to accept the corresponding traffic, it has also to inform the directly connected routers (any local router) sending a join message.
 2. The local routers will keep this information and it has to arrange to provide related traffic to all local multicast group members.
 3. The router must also check if there still members of specific multicast group by sending periodically queries. If someone respond to the query, the multicast traffic will continue to be forwarded.
 4. If there is nobody that respond to the query, the router can decide after a while to stop forwarding traffic related to a specific multicast group.


**Standard**

Versions : IGMPv1 :rfc:`1112`, IGMPv2 :rfc:`2236`, IGMPv3 :rfc:`3376`

.. figure:: imgs/igmp-message.png
   :align: center
   :scale: 100

   Structure of an IGMP message

..

The IGMP message size is 8 bytes, the IGMP messages are sent in the IP datagrams payload with a protocol number of 2. The IGMP message consists of 4 fields :

 * Type : the type of the message (e.g. Membership query (0x11), membership report (0x12), and leave message (0x17))
 * Maximum response : maximum time until the report has to be sent in response to corresponding query
 * Checksum : applied on the message using the same way as in an ICMP message
 * Group address : contains the IP multicast address or is set to zero in case of query.

The IGMP message has a TTL set to 1, so it cannot be forwarded by local routers.

.. figure:: imgs/igmp-query-report.png
   :align: center
   :scale: 100

   IGMP querie and report examples

..

.. rubric:: References

.. [#figmpjoin] http://www.cisco.com
