.. sectionauthor:: Mallory Declercq

Any-Source and Source-Specific Multicast
========================================

Any-Source Multicast
--------------------

ASM is an IP multicast service model in which receiver indicates that it can receive traffic from any-source. The only thing that the receiver knows is the multicast group address that the sender would be using. This specification may lead to the case where unwanted traffic comes to a receiver because the receiver doesn't know the source and so he receives traffic from all sources for a multicast group.

In this model, end-hosts may join and leave the group any time. There is no restriction on their location or number. Moreover, this model supports multicast groups with arbitrarily many senders (any end-host may transmit to a host group, even if it is not a member of that group) ASM supports both one-to-many and many-to-many communication models. Radio and television are good examples of the one-to-many model and online gaming and videoconferencing are examples of the many-to-many model. In many-to-many model, some or all the participants can become sources which means that the network is responsible for source discovery.

Here is an example of ASM where two receivers (R1 and R2) want to join the multicast group :

.. figure:: imgs/asm-example.png
   :align: center
   :scale: 100

   2 hosts joining a multicast group

.. note:: With ASM, the `Join` message only requires the destination address as parameter.


Source-Specific Multicast
-------------------------

SSM is an IP multicast service model that supports multicast delivery from only one specified source to its receivers. Compared to ASM, SSM only supports the one-to-many communication models. In this model, the set of multicast hosts are identify by a group and a source address. An SSM group, called a channel, is identified as a (S, G) pair where S is the source address and G is the group address. Only source-based forwarding trees are needed to implement this model. As no Rendezvous point is needed, the shortest path from sender to receiver is used by the multicast traffic.

Here are some benefits of SSM over ASM :

 - Group adresses can be reused by multiple sources while keeping channels unique. It means that if an host subscribes to a specific channel, he will not receive traffic from others channels. This feature prevents hosts to receive unwanted traffic.
 - SSM does not rely on the designation of a Rendezvous point to establish a tree infrastructure. Thus the complexity of the multicast routing infrastructure is low.

Here is an example of SSM where two receivers (R1 and R2) want to join goup G and source S1 :

.. figure:: imgs/ssm-example.png
   :align: center
   :scale: 100

   2 hosts joining multicast group G1 and source S1

.. note:: With SSM, the `Join` message requires both the source address and the destination/group address as parameter.

Receiver 1 and 2 send their join messages to the source by following the shortest path. This procedure will build the source tree hop by hop until it reaches the source. By using the source tree, the multicast traffic can be delivered to the subscribing hosts (receiver 1 and 2).

