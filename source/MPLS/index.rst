.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. Use the sectionauthor directive to indicate the contributor to each section

Multiprotocol Label Switching
=============================

This chapter will describe the basic principles of MPLS and the LDP protocol.


.. toctree::
   :maxdepth: 2
   
   Theory of MPLS <theory>
   
   Multiple choice questions <mcq>

   Experimenting with MPLS <lab>
