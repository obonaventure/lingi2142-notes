Experimenting with MPLS
-----------------------
.. sectionauthor:: Mathieu Jadin

Netkit can be used to manipulate MPLS networks. You will use the lab :download:`/netkit/netkit-lab_mpls_forwarding.zip` in order to experiment with the MPLS manual configuration of a network.

The lab consists of 4 hosts and 8 routers as shown below :

	.. tikz:: The topology of the netkit lab (with p=2001:db8:be::/32)
		:libs: positioning, matrix, arrows
		
		\tikzstyle{arrow} = [thick,->,>=stealth]
		\tikzset{host/.style = {circle, draw, text centered, minimum height=2em}, }
		\tikzset{router/.style = {rectangle, draw, text centered, minimum height=2em}, }
		
		
		\matrix[row sep=2.6cm, column sep=2.6cm] {
			\node[host] (hosta) {hosta}; & \node[router] (lsr7) {lsr7}; & & \node[router] (lsr8) {lsr8}; & \node[host] (hostd) {hostd};\\
			 & & \node[router] (lsr6) {lsr6}; & \node[router] (lsr5) {lsr5}; & \\
			\node[host] (hostb) {hostb}; & \node[router] (lsr1) {lsr1}; & \node[router] (lsr2) {lsr2}; & \node[router] (lsr4) {lsr4}; & \\
			 & & \node[router] (lsr3) {lsr3}; & \node[host] (hostc) {hostc}; & \\
		};
		
		\draw[thick] (hosta) -- (lsr7) node[very near start,sloped,above] {\scriptsize{eth0}} node[very near end,sloped,above] {\scriptsize{eth0}} node[near start,sloped,below] {\scriptsize{p:a7::a0}} node[near end,sloped,below] {\scriptsize{p:a7::70}};
		
		\draw[thick] (lsr7) -- (lsr6) node[near start,sloped,above] {\scriptsize{eth1}} node[near end,sloped,above] {\scriptsize{eth0}} node[near start,sloped,below] {\scriptsize{p:67::71}} node[near end,sloped,below] {\scriptsize{p:67::60}};
		
		\draw[thick] (hostb) -- (lsr1) node[very near start,sloped,above] {\scriptsize{eth0}} node[very near end,sloped,above] {\scriptsize{eth1}} node[near start,sloped,below] {\scriptsize{p:b1::b0}} node[near end,sloped,below] {\scriptsize{p:b1::11}};
		
		\draw[thick] (lsr1) -- (lsr6) node[near start,sloped,above] {\scriptsize{eth0}} node[near end,sloped,above] {\scriptsize{eth1}} node[near start,sloped,below] {\scriptsize{p:16::10}} node[near end,sloped,below] {\scriptsize{p:16::61}};
		
		\draw[thick] (lsr1) -- (lsr2) node[very near start,sloped,above] {\scriptsize{eth2}} node[very near end,sloped,above] {\scriptsize{eth0}} node[near start,sloped,below] {\scriptsize{p:12::12}} node[near end,sloped,below] {\scriptsize{p:12::20}};
		
		\draw[thick] (lsr2) -- (lsr3) node[very near start,sloped,above] {\scriptsize{eth2}} node[very near end,sloped,above] {\scriptsize{eth0}} node[near start,sloped,below] {\scriptsize{p:23::21}} node[near end,sloped,below] {\scriptsize{p:23::30}};
		
		\draw[thick] (lsr2) -- (lsr4) node[very near start,sloped,above] {\scriptsize{eth1}} node[very near end,sloped,above] {\scriptsize{eth1}} node[near start,sloped,below] {\scriptsize{p:24::21}} node[near end,sloped,below] {\scriptsize{p:24::41}};
		
		\draw[thick] (lsr3) -- (hostc) node[very near start,sloped,above] {\scriptsize{eth1}} node[very near end,sloped,above] {\scriptsize{eth0}} node[near start,sloped,below] {\scriptsize{p:c3::31}} node[near end,sloped,below] {\scriptsize{p:c3::c0}};
		
		\draw[thick] (lsr5) -- (lsr4) node[very near start,sloped,above] {\scriptsize{eth1}} node[very near end,sloped,above] {\scriptsize{eth0}} node[near start,sloped,below] {\scriptsize{p:45::51}} node[near end,sloped,below] {\scriptsize{p:45::40}};
		
		\draw[thick] (lsr5) -- (lsr6) node[very near start,sloped,above] {\scriptsize{eth0}} node[very near end,sloped,above] {\scriptsize{eth3}} node[near start,sloped,below] {\scriptsize{p:56::50}} node[near end,sloped,below] {\scriptsize{p:56::63}};
		
		\draw[thick] (lsr6) -- (lsr8) node[near start,sloped,above] {\scriptsize{eth2}} node[near end,sloped,above] {\scriptsize{eth0}} node[near start,sloped,below] {\scriptsize{p:68::62}} node[near end,sloped,below] {\scriptsize{p:68::80}};
		
		\draw[thick] (hostd) -- (lsr8) node[very near start,sloped,above] {\scriptsize{eth0}} node[very near end,sloped,above] {\scriptsize{eth1}} node[near start,sloped,below] {\scriptsize{p:d8::d0}} node[near end,sloped,below] {\scriptsize{p:d8::81}};

There is already a MPLS path that is configured between `hosta` and `hostb` (see below).

1. Analyze the traffic (with tcpdump) that is observed at the routers (`lsr1`, `lsr6` and `lsr7`) and at the two hosts (`hosta` and `hostb`) when you ping `hosta` from `hostb`. Identify which labels are used at each link.
	
	.. tikz:: Path already configured in the lab
		:libs: positioning, matrix, arrows
		
		\tikzstyle{arrow} = [thick,->,>=stealth]
		\tikzset{host/.style = {circle, draw, text centered, minimum height=2em}, }
		\tikzset{router/.style = {rectangle, draw, text centered, minimum height=2em}, }
		
		
		\matrix[row sep=1cm, column sep=1cm] {
			\node[host] (hosta) {hosta}; & \node[router] (lsr7) {lsr7}; & \\
			 & & \node[router] (lsr6) {lsr6};\\
			\node[host] (hostb) {hostb}; & \node[router] (lsr1) {lsr1}; & \\
		};
		
		\draw[thick] (hosta) -- (lsr7) node[midway,sloped,above] {$\longrightarrow$} node[midway,sloped,below] {$\longleftarrow$};
		
		\draw[thick] (lsr7) -- (lsr6) node[midway,sloped,above] {$\longrightarrow$} node[midway,sloped,below] {$\longleftarrow$};
		
		\draw[thick] (hostb) -- (lsr1) node[midway,sloped,above] {$\longrightarrow$} node[midway,sloped,below] {$\longleftarrow$};
		
		\draw[thick] (lsr1) -- (lsr6) node[midway,sloped,above] {$\longrightarrow$} node[midway,sloped,below] {$\longleftarrow$};


MPLS in Linux
^^^^^^^^^^^^^

In order to use MPLS, the `LSRs` first load the modules mpls6 (for IPv-4, it is mpls4), mplsbr and mpls_tunnel.

Three structures are used in the Linux implementation of MPLS. The command lines that are used to manipulate them will be explained below.

- The Next Hop Label Forwarding Entry (NHLFE) table that stores in each row :

	- the instructions to apply (e.g. push a label on the stack and/or forward to another row of the table)
	- the next hop where the packet should be sent to.

- The Incoming Label Map (ILM) that stores all the labels that are accepted by the LSR.

- The Forwarding Equivalence Classes (FECs) that are also stored in the LSR.

Edge LSRs
.........

The egress LSRs have to do two more things than backbone LSRs (see below) :

- Match a particular FEC to a particular label, a nexthop and an operation.

- Pop all the labels of packets that should leave the domain.

In the lab, the edge LSRs are `lsr1`, `lsr3`, `lsr7` and `lsr8`.

In `lsr1`, all packets with `hosta` as destination are considered as an FEC. This FEC is mapped to an entry of the NHLFE table. All the packets of this FEC will be labelled with 2001 and they will be forwarded to `lsr6` through `eth0`. The command lines are the following :

.. code-block:: bash
	
	# The following line will add an entry
	# in the Next Hop Label Forwarding Entry (NHLFE) table.
	# The packets matching this entry will see the label 2001 pushed on their stack
	# and they will be forwarded to 2001:db8:be:16::61 through eth0.
	# The key of the entry is stored in a variable.
	var=`mpls nhlfe add key 0 instructions push gen 2001
			next hop eth0 ipv6 2001:db8:be:16::61 | grep key | cut -c 17-26`

	# We define a FEC for all packets directed to 2001:db8:be:A7::A0/64.
	# The corresponding label can be found by looking at the entry defined above.
	# We use the key returned by the previous instruction
	# to do the matching between the FEC and the entry in the NHLFE table.
	ip -6 route add 2001:db8:be:A7::A0/64 via 2001:db8:be:16::61 mpls $var

In `lsr1`, all packets with label 1002 that are arriving at the `eth0` interface will see their label popped. They will then be forwarded to their destination. The command lines are the following :

.. code-block:: bash
	
	# eth0 is set to receive mpls traffic
	mpls labelspace set dev eth0 labelspace 0

	# Add an entry on Icoming Label Map (ILM) in order to list the incoming label (1002)
	# The label will be popped.
	mpls ilm add label gen 1002 labelspace 0 proto ipv6

Backbone LSRs
.............

The backbone LSRs have to look, for every label they receive (if they are in the ILM), the actions to perform in their NHLFE table.

In the lab, the backbone LSRs are `lsr2`, `lsr4`, `lsr5` and `lsr6`.

In `lsr1`, all packets that have the label 1001 on the top of their stack will be forwarded to `lsr1` and this label will be swapped wih the label 1002. The command lines are the following :

.. code-block:: bash
	
	# eth1 is set to receive mpls traffic
	mpls labelspace set dev eth1 labelspace 0
	
	# Add an entry on Icoming Label Map (ILM) for 1001
	mpls ilm add label gen 1001 labelspace 0 proto ipv6
	
	# The following line will add an entry
	# in the Next Hop Label Forwarding Entry (NHLFE) table.
	# The packets matching this entry will see the label 1002 pushed on their stack
	# and they will be forwarded to 2001:db8:be:16::10 through eth1.
	# The key of the entry is stored in a variable.
	var2=`mpls nhlfe add key 0 instructions push gen 1002
			nexthop eth1 ipv6 2001:db8:be:16::10 | grep key | cut -c 17-26`
	
	# This line maps the incomming label 1001 to the entry of the NHLFE table
	# (with the key returned by the previous command line).
	mpls xc add ilm_label gen 1001 ilm_labelspace 0 nhlfe_key $var2

Special usages
..............

Here are some special combinations of the commands presented above that you will need to do the exercises. [#f1]_

.. code-block:: bash

	# These three command lines are used to swap label 100 with 200
	# and then push 2000 on the top of the stack.
	var1=`mpls nhlfe add key 0 instructions push gen 2000
			nexthop eth1 ipv6 2001:db8:be:16::10 | grep key | cut -c 17-26`
	var2=`mpls nhlfe add key 0 instructions push gen 200 forward $var1
			| grep key | cut -c 17-26`
	mpls xc add ilm_label gen 100 ilm_labelspace 0 nhlfe_key $var2

.. code-block:: bash

	# These four command lines are used to pop the label 3000 and then swap 200 with 300.
	mpls labelspace set dev eth1 labelspace 0
	mpls ilm add label gen 3000 labelspace 0
	mpls ilm add label gen 200 labelspace 0
	key=`mpls nhlfe add key 0 instructions push gen 300
			nexthop eth1 ipv6 2001:db8:be:16::10 | grep key | cut -c 17-26`
	mpls xc add ilm_label gen 200 ilm_labelspace 0 nhlfe_key $key

Assignments
^^^^^^^^^^^

Now let's move on the serious stuff ! 

2. Modify the startup scripts of the LSRs in order to enable `hostb` to ping `hostd`. The path taken by the packets must be `hostb` - `lsr1` - `lsr2` - `lsr4` - `lsr5` - `lsr6` - `lsr8` - `hostd`.

3. Modify the startup scripts of the LSRs in order to enable `hostc` to ping `hosta`. The path taken by the packets must be `hostc` - `lsr3` - `lsr2` - `lsr4` - `lsr5` - `lsr6` - `lsr7` - `hosta`.

Popping the last label of the stack in the edge LSR needs two lookups: one in the ILM and one in the routing table to know where to send it. We can do better by doing a penultimate popping (popping the last label at the penultimate LSR).

4. Do penultimate popping for the three paths configured.

5. Create a tunnel starting at LSR2 and ending at LSR5 for the two paths you have implemented. Check that your implementation works by doing by analyzing traffic with tcpdump.

6. Do penultimate popping in your tunnel.


.. rubric:: Footnotes

.. [#f1] If you want more examples about the usage of MPLS, you can download them in `MPLS-LINUX website <http://sourceforge.net/projects/mpls-linux/files/mpls-linux/Documentation/MPLS%20Linux%20Labs/>`_.
