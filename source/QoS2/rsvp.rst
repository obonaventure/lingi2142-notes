.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. Use the sectionauthor directive to indicate the contributor to each section

The ReSource reserVation Protocol
---------------------------------
.. sectionauthor:: Hamaide Mickael and Gerard Kevin

============
Introduction
============
RSVP stands for Resource ReserVation Protocol. This is a transport layer (layer 4) protocol used to reserve resources across a network. This reservation is used to provide to a particular flow a specified QoS and is initiated by the receiver. It may request for a maximum end-to-end delay, a minimal amount of bandwidth or both at the same time and can be used for multicast or unicast data flows. It does not transport user data directly, but rather provides a control scheme which application can use to request some resources in the network. It works with both IPv4 and IPv6 and is not a routing protocol meaning that it is compatible with any existing network (as long as all the routers on the way from the sender to the receiver understand RSVP messages). 

It can be used along with integrated services IntServ. 

===========================
Description of the protocol
===========================
A reservation is placed for a data flow, this flow being identified by :

 - its destination address which can be a unicast or a multicast address
 - its protocol ID (UDP, TCP, ...)
 - its destination port which is optional
The reservation is only valid in one direction (from the source to the receiver) meaning that if the hosts want to achieve QoS in both ways they will have to make a reservation once in every way. 

The reservation model is split into two different parts :

 - a flowspec: the specification of the desired QoS (the amount of bandwidth requested or the maximum end-to-end delay) which will set the parameters of the packet scheduler
 - a filterspec: used with a session specification, it will define the format of the flow of packets considered for the flowspec and will set the parameters of the packet classifier.


Flowspec
========
Flowspec may be divided itself into two different parts :
 - TSpec for Traffic Specification: will tell how the traffic should look like. They typically include token bucket's parameters. This token bucket has a depth which will describe how bursty the traffic can be (that is how many packets can come at once and be offered QoS). The second parameter is the rate of arrival of token which enforce the average rate of the traffic. Both these parameters are used to ensure that the traffic respects the given specifications. If the traffic does not match these specifications then it is either dropped or managed using best effort.

 - RSpec for Reservation Specification: will specify the reservation that is made for the flow. It can simply be to use internet best effort, in which case the reservation is not needed, or 'controlled load' which ensures an average maximum delay (see integrated services section for more details), or finally 'Guaranteed' which ensures a strict maximum delay for as long as the traffic respects its specifications (see integrated services section for more details).


=========
Operation
=========

The source who wants to be able to request a certain QoS will send PATH messages to the receiver. These messages will travel in the network as normal IP packets, that is they will follow the same way as other IP packets such as computed by the routing policies that are currently deployed. Each time a PATH message reaches a router, the router will put its identifier in the packet as Previous Hop. 
A PATH message will contain the following information : 
 - Sender template : this describes the format of data packets that will correspond to the flow (the filterspec).
 - Sender TSpec: (See the above description of TSpec).
 - Adspec : for advertising purposes.

.. figure:: rsvp-examplephop.png
    :scale: 70%
    :align: center
    
    We can see here on the figure the way followed by PATH messages and the data.


Upon receival of a PATH message a router will create a path state containing the previous hop (green letters below the routers on the previous figure) and will update the same value contained in the PATH message, so that the reply (RESV message) can follow the exact reverse of the PATH message by simply going from previous hop to previous hop. The RESV message will thus be guided through the network by routers using these path states. 
If one of the routers on the way from the receiver to the sender does not understand RSVP, it will simply ignore the protocol and will process the packet as a normal IP packet. 

The RESV message is a reply to a PATH message. It contains the flowspec and filterspec for the desired QoS on a flow. This message will always follow the exact reverse of the PATH message and every router on the network that is able to understand RSVP will create a soft state for that reservation. So each router will need to store the nature of the flow and will police it. This soft state may evolve to reflect the new reservation parameters as hosts may revised these by sending updated RESV messages containing 

.. figure:: rsvp-example.png
    :scale: 70%
    :align: center
    
    We can see on this schema the path followed by PATH message and their corresponding RESV replies.


As each reservation is stored in soft state it needs a periodic refresh, meaning that as long as the receiver wants to maintain its reservation, it has to send path messages. The advantage of that refresh is that the routers on the way may quickly react and automatically adapt themselves to network failures and route changes. This also allows the network to delete reservations if one of the hosts comes to crash or is not reachable anymore. The default timeout is 30 seconds in order not to overflow the network with PATH messages and to react rather quickly to failures.

There exists two reasons for which a reservation may evolve as it travels towards the receiver:

 - The traffic control mechanism could modify a flowspec hop-by-hop
 - several reservations from different branches of the multicast tree may be merged 

What about link failures ?
==========================
In case of a link failure on the path from the receiver to the sender, the soft state maintained in the routers will timeout. The network will recompute the routing tables with the network routing protocol (for instance OSPF). A new path from the sender to the receiver will be available, PATH messages will be sent via the new route and new soft states will be created. As the PATH messages follow the new path, routers that were on the path but are not anymore will simply timeout and remove the soft states.

What if the reservation is refused ? 
====================================
If one of the routers along the way is not able to feed the reservation it will send an error notification to the receiver who initiated the message. This error is propagated downstream (towards the receiver) but does not delete reservations on previous hops. This deletion will either be the result of a timeout or of the sending of a teardown message which will immediately cancel the corresponding reservation and lead to the deletion of the soft state.

.. Reservation styles
   ==================
   The style of the reservation is a set of options which may contain :

   - the treatment of reservations for different senders within the same session: either to establish a distinct reservation for each sender or make a single shared reservation between all senders.
   - the selection of senders : it could be an explicit list of senders or a wildcard to select all senders. 

    Here's a table that will define all three styles of reservation that are now defined in RSVP [:rfc:`2205`] :

    +--------------------+------------------+-----------------+
    |  Sender selection  |      Distinct    |      Shared     |
    +====================+==================+=================+
    |                    |                  |                 |
    |     Explicit       |   Fixed Filter   | Shared Explicit |
    |                    |       FF         |        SE       |
    |                    |                  |                 |
    +--------------------+------------------+-----------------+
    |                    |                  |                 |
    |      Wildcard      |       None       | Wildcard Filter |
    |                    |                  |       WF        |
    |                    |                  |                 |
    +--------------------+------------------+-----------------+

     - WF: this style implies the option shared reservation and a wildcard to select all senders. It will thus extend any reservation to new senders as they join the group. 
     - SE: this style implies the option shared reservation and an explicit selection of senders. It creates a single reservation for all explicitly selected senders and needs to be explicitly modified to add new senders. 
     - FF: this style implies distinct reservation for each of the explicitly selected senders. The total reservation on a link for a given session is thus the sum of all flowspec (one for each sender). 

===========
Scalability
===========
As every router along the path between two hosts needs to store a state for the reservation, this protocol will work well in small-scale networks but is really hard to extend to large-scale ones. 

.. One of the solution to this issue could be to perform multilevel reservation : the routers on the edge of the network could maintain states for every microtransaction they will need to process and to  offer a certain QoS to, and router in the core of the network could only maintain states for aggregate flows that go through them. 

========
Overhead
========
As RSVP operates on layer 4, it creates a signaling overhead which will need to be processed by each router in the network. Moreover, as the reservation needs to be refreshed it may increase the traffic load. 


References

 - :rfc:`2205` for the protocol specification (see also :rfc:`2750` for the policies)
 - Zhang, Lixia, Steve Deering, Deborah Estrin, Scott Shenker, and Daniel Zappala. "RSVP: A new resource reservation protocol." Network, IEEE 7, no. 5 (1993): 8-18.
 - Resource ReserVation Protocol, http://en.wikipedia.org/wiki/Resource_Reservation_Protocol as lastly retrieved by May 6th 2015

=========
Exercises
=========

Question 1
==========


.. question:: RESVpath 
	
	.. figure:: rsvp-exercice.png
		:scale: 70%
		:align: center
		
	What is the path followed by RESV message in the above figure ?
	
	.. negative:: R6-R5-R3-R4-R2.
		
	.. negative:: R6-R5-R3-R1-R2.

	.. positive:: R6-R3-R4-R2.
	
	.. negative:: R6-R3-R1-R2.

Question 2
==========


.. question:: PATHAF
    
     The link R3-R4 has failed. What is the path followed by the RESV messages after that failure ? 
     
     .. negative:: R6-R5-R3-R1-R2
     
     .. negative:: R6-R3-R1-R2
     
     .. negative:: R3 sends RESVErr message to B
     
     .. positive:: R1 sends RESVErr message to B

